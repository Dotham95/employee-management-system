package com.vti.controller;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vti.dto.timesheet.TimesheetDTO;
import com.vti.entity.Timesheet;
import com.vti.service.timesheet.ITimesheetService;

@RestController
@RequestMapping(value = "api/v1/timesheets")
@CrossOrigin("*")
public class TimesheetController {
	
	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private ITimesheetService service;

	@GetMapping()
	public Page<TimesheetDTO> getAllTimesheets(Pageable pageable) {

		Page<Timesheet> entityPages = service.getAllTimesheets(pageable);

		// convert entities --> dtos
		List<TimesheetDTO> dtos = modelMapper.map(entityPages.getContent(), new TypeToken<List<TimesheetDTO>>() {
		}.getType());

		Page<TimesheetDTO> dtoPages = new PageImpl<>(dtos, pageable, entityPages.getTotalElements());

		return dtoPages;
	}

//	@GetMapping(value = "/{id}")
//	public Timesheet getTimesheetByID(@PathVariable(name = "id") int id) {
//		return service.getTimesheetByID(id);
//	}
//
//	@PostMapping()
//	public void createTimesheet(@RequestBody TimesheetForm form) {
//		service.createTimesheet(form.toEntity());
//	}
//
//	@PutMapping(value = "/{id}")
//	public void updateTimesheet(@PathVariable(name = "id") int id, @RequestBody TimesheetForm form) {
//		Timesheet Timesheet = form.toEntity();
//		Timesheet.setId(id);
//		service.updateTimesheet(Timesheet);
//	}
//
//	@DeleteMapping(value = "/{id}")
//	public void deleteTimesheet(@PathVariable(name = "id") int id) {
//		service.deleteTimesheet(id);
//	}
}
