package com.vti.service.accounting;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.vti.entity.Accounting;

public interface IAccountingService {

	public Page<Accounting> getAllAccountings(Pageable pageable);

	public Accounting getAccountingByID(int id);

//	public void createAccounting(Accounting accounting);
//
//	public void updateAccounting(Accounting accounting);
//
//	public void deleteAccounting(int id);
//
//	public boolean isAccountingExistsByID(int id);
}
