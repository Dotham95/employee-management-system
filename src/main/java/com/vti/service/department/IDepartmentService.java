package com.vti.service.department;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.vti.entity.Department;

public interface IDepartmentService {

	public Page<Department> getAllDepartments(Pageable pageable);

//	public Department getDepartmentByID(int id);
//
//	public void createDepartment(Department Department);
//
//	public void updateDepartment(Department Department);
//
//	public void deleteDepartment(int id);
//
//	public boolean isDepartmentExistsByID(int id);
}
