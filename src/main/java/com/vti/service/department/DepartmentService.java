package com.vti.service.department;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.vti.entity.Department;
import com.vti.repository.IDepartmentRepository;

@Service
public class DepartmentService implements IDepartmentService {

	@Autowired
	private IDepartmentRepository repository;

	public Page<Department> getAllDepartments(Pageable pageable) {
		return repository.findAll(pageable);
	}

//	public Department getDepartmentByID(int id) {
//		return repository.findById(id).get();
//	}
//
//	public void createDepartment(Department Department) {
//		repository.save(Department);
//	}
//
//	public void updateDepartment(Department Department) {
//		repository.save(Department);
//	}
//
//	public void deleteDepartment(int id) {
//		repository.deleteById(id);
//	}
//
//	public boolean isDepartmentExistsByID(int id) {
//		return repository.existsById(id);
//	}

}
