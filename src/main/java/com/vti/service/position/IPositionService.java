package com.vti.service.position;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.vti.entity.Position;

public interface IPositionService {

	public Page<Position> getAllPositions(Pageable pageable);

//	public Position getPositionByID(int id);
//
//	public void createPosition(Position Position);
//
//	public void updatePosition(Position Position);
//
//	public void deletePosition(int id);
//
//	public boolean isPositionExistsByID(int id);
}
