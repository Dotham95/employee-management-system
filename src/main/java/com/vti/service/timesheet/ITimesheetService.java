package com.vti.service.timesheet;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.vti.entity.Timesheet;

public interface ITimesheetService {

	public Page<Timesheet> getAllTimesheets(Pageable pageable);

//	public Timesheet getTimesheetByID(int id);
//
//	public void createTimesheet(Timesheet Timesheet);
//
//	public void updateTimesheet(Timesheet Timesheet);
//
//	public void deleteTimesheet(int id);
//
//	public boolean isTimesheetExistsByID(int id);
}
