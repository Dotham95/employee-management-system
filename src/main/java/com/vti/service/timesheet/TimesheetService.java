package com.vti.service.timesheet;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.vti.entity.Timesheet;
import com.vti.repository.ITimesheetRepository;

@Service
public class TimesheetService implements ITimesheetService {

	@Autowired
	private ITimesheetRepository repository;

	public Page<Timesheet> getAllTimesheets(Pageable pageable) {
		return repository.findAll(pageable);
	}

//	public Timesheet getTimesheetByID(int id) {
//		return repository.findById(id).get();
//	}
//
//	public void createTimesheet(Timesheet Timesheet) {
//		repository.save(Timesheet);
//	}
//
//	public void updateTimesheet(Timesheet Timesheet) {
//		repository.save(Timesheet);
//	}
//
//	public void deleteTimesheet(int id) {
//		repository.deleteById(id);
//	}
//
//	public boolean isTimesheetExistsByID(int id) {
//		return repository.existsById(id);
//	}

}
