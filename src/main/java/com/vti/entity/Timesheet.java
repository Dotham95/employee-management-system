package com.vti.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@Table(name = "Timesheet")
public class Timesheet implements Serializable {

private static final long serialVersionUID = 1L;
	
	@Column(name = "id")
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@ManyToOne
	@JoinColumn(name = "employee_id",nullable = false)
	private Employee employee;
	
	@Column(name = "time_in")
	@Temporal(TemporalType.TIMESTAMP)
	@CreationTimestamp
	private Date timeIn;
	
	@Column(name = "time_out")
	@Temporal(TemporalType.TIMESTAMP)
	@CreationTimestamp
	private Date timeOut;
	
	@Column(name = "working_time",nullable = false)
	private int workingTime;
	
	@Column(name = "`status` ", length = 50, nullable = false)
	private String status;
	
}
