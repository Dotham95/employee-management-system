package com.vti.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@Table(name = "Accounting")
@Inheritance(strategy = InheritanceType.JOINED)
public class Accounting {

private static final long serialVersionUID = 1L;
	
	@Column(name = "accounting_id")
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@OneToOne
	@JoinColumn(name = "employee_id",referencedColumnName = "employee_id")
	private Employee employee;
	
	@Column(name = "basic_salary", nullable = false)
	private int basicSalary;
	
	@Column(name = "money_for_meals", nullable = false)
	private int moneyForMeals;
	
	@Column(name = "responsibility")
	private int responsibility;
	
	@Column(name = "seniority")
	private int seniority;
	
	@Column(name = "bonus")
	private int bonus;

	@Column(name = "total_working_hours")
	private int totalWorkingHours;
	
	@Column(name = "total_overtime_hours")
	private int totalOvertimeHours;
	
	@Column(name = "personal_income_tax")
	private int personalIncomeTax;

	@Column(name = "advance")
	private int advance;
	
	@Column(name = "total_salary")
	private int totalSalary;

}
